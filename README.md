Описание выполения работы
=========================

[Рефакторинг и смена порта приложения для macOS](https://gitlab.com/otus-sre1/flask-sqlite3-todo-crud/-/commit/1976d77c02c8c216ddfeb5f71e38518ad4e5600f)

1.

[Добавление flask-экспортера](https://gitlab.com/otus-sre1/flask-sqlite3-todo-crud/-/commit/b9e5f8d02e7a90541adef5916527f1e921cd2d2e)

По пути <http://127.0.0.1:5010/metrics> отдаются метрики в формате Prometheus, такие как:  
`flask_http_request_duration_seconds`  
`flask_http_request_total`  
`flask_http_request_exceptions_total`  
`flask_exporter_info`

```shell
# HELP python_gc_objects_collected_total Objects collected during gc
# TYPE python_gc_objects_collected_total counter
python_gc_objects_collected_total{generation="0"} 529.0
python_gc_objects_collected_total{generation="1"} 73.0
python_gc_objects_collected_total{generation="2"} 0.0
# HELP python_gc_objects_uncollectable_total Uncollectable object found during GC
# TYPE python_gc_objects_uncollectable_total counter
python_gc_objects_uncollectable_total{generation="0"} 0.0
python_gc_objects_uncollectable_total{generation="1"} 0.0
python_gc_objects_uncollectable_total{generation="2"} 0.0
# HELP python_gc_collections_total Number of times this generation was collected
# TYPE python_gc_collections_total counter
python_gc_collections_total{generation="0"} 143.0
python_gc_collections_total{generation="1"} 12.0
python_gc_collections_total{generation="2"} 1.0
# HELP python_info Python platform information
# TYPE python_info gauge
python_info{implementation="CPython",major="3",minor="11",patchlevel="3",version="3.11.3"} 1.0
# HELP flask_exporter_info Information about the Prometheus Flask exporter
# TYPE flask_exporter_info gauge
flask_exporter_info{version="0.22.4"} 1.0
# HELP flask_http_request_duration_seconds Flask HTTP request duration in seconds
# TYPE flask_http_request_duration_seconds histogram
flask_http_request_duration_seconds_bucket{le="0.005",method="GET",path="/",status="200"} 1.0
flask_http_request_duration_seconds_bucket{le="0.01",method="GET",path="/",status="200"} 2.0
flask_http_request_duration_seconds_bucket{le="0.025",method="GET",path="/",status="200"} 2.0
flask_http_request_duration_seconds_bucket{le="0.05",method="GET",path="/",status="200"} 2.0
flask_http_request_duration_seconds_bucket{le="0.075",method="GET",path="/",status="200"} 2.0
flask_http_request_duration_seconds_bucket{le="0.1",method="GET",path="/",status="200"} 2.0
flask_http_request_duration_seconds_bucket{le="0.25",method="GET",path="/",status="200"} 2.0
flask_http_request_duration_seconds_bucket{le="0.5",method="GET",path="/",status="200"} 2.0
flask_http_request_duration_seconds_bucket{le="0.75",method="GET",path="/",status="200"} 2.0
flask_http_request_duration_seconds_bucket{le="1.0",method="GET",path="/",status="200"} 2.0
flask_http_request_duration_seconds_bucket{le="2.5",method="GET",path="/",status="200"} 2.0
flask_http_request_duration_seconds_bucket{le="5.0",method="GET",path="/",status="200"} 2.0
flask_http_request_duration_seconds_bucket{le="7.5",method="GET",path="/",status="200"} 2.0
flask_http_request_duration_seconds_bucket{le="10.0",method="GET",path="/",status="200"} 2.0
flask_http_request_duration_seconds_bucket{le="+Inf",method="GET",path="/",status="200"} 2.0
flask_http_request_duration_seconds_count{method="GET",path="/",status="200"} 2.0
flask_http_request_duration_seconds_sum{method="GET",path="/",status="200"} 0.008962918003817322
# HELP flask_http_request_duration_seconds_created Flask HTTP request duration in seconds
# TYPE flask_http_request_duration_seconds_created gauge
flask_http_request_duration_seconds_created{method="GET",path="/",status="200"} 1.686117294108063e+09
# HELP flask_http_request_total Total number of HTTP requests
# TYPE flask_http_request_total counter
flask_http_request_total{method="GET",status="200"} 2.0
# HELP flask_http_request_created Total number of HTTP requests
# TYPE flask_http_request_created gauge
flask_http_request_created{method="GET",status="200"} 1.686117294108115e+09
# HELP flask_http_request_exceptions_total Total number of HTTP requests which resulted in an exception
# TYPE flask_http_request_exceptions_total counter
```

2.
[Добавление конфигурации json-экспортера](https://gitlab.com/otus-sre1/flask-sqlite3-todo-crud/-/commit/136fe91848b005d05b325051a38b5102e65bb189)

```shell
docker container run --rm -p 7979:7979 -v $PWD/ISS-Current-Location/config.yml:/config.yml quay.io/prometheuscommunity/json-exporter --config.file=/config.yml
```

По пути <http://localhost:7979/probe?target=http://api.open-notify.org/iss-now.json> получаем метрики в формате Prometheus

```shell
# HELP iss_position_latitude International Space Station Current Location
# TYPE iss_position_latitude untyped
iss_position_latitude -32.5891
# HELP iss_position_longitude International Space Station Current Location
# TYPE iss_position_longitude untyped
iss_position_longitude 2.0747
# HELP iss_timestamp_timestamp Timestamp
# TYPE iss_timestamp_timestamp untyped
iss_timestamp_timestamp 1.686117583e+09
```
